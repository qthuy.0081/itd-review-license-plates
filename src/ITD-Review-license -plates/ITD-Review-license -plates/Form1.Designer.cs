﻿namespace ITD_Review_license__plates
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtgvPlate = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbFilter = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblTransaction = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ptbMain = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvPlate)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbMain)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgvPlate
            // 
            this.dtgvPlate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvPlate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvPlate.Location = new System.Drawing.Point(3, 71);
            this.dtgvPlate.Name = "dtgvPlate";
            this.dtgvPlate.RowHeadersWidth = 51;
            this.dtgvPlate.Size = new System.Drawing.Size(308, 565);
            this.dtgvPlate.TabIndex = 0;
            this.dtgvPlate.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgvPlate_CellEnter);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbFilter);
            this.panel1.Controls.Add(this.dtgvPlate);
            this.panel1.Location = new System.Drawing.Point(6, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(314, 636);
            this.panel1.TabIndex = 3;
            // 
            // cbFilter
            // 
            this.cbFilter.AutoSize = true;
            this.cbFilter.Location = new System.Drawing.Point(7, 48);
            this.cbFilter.Name = "cbFilter";
            this.cbFilter.Size = new System.Drawing.Size(67, 17);
            this.cbFilter.TabIndex = 3;
            this.cbFilter.Text = "<6 Digits";
            this.cbFilter.UseVisualStyleBackColor = true;
            this.cbFilter.CheckedChanged += new System.EventHandler(this.cbFilter_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblTransaction);
            this.panel2.Controls.Add(this.lblDate);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Location = new System.Drawing.Point(323, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(929, 635);
            this.panel2.TabIndex = 4;
            // 
            // lblTransaction
            // 
            this.lblTransaction.AutoSize = true;
            this.lblTransaction.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransaction.Location = new System.Drawing.Point(643, 16);
            this.lblTransaction.Name = "lblTransaction";
            this.lblTransaction.Size = new System.Drawing.Size(81, 16);
            this.lblTransaction.TabIndex = 2;
            this.lblTransaction.Text = "TransactionId";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(17, 16);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(32, 16);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "Date";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.ptbMain);
            this.panel5.Location = new System.Drawing.Point(20, 71);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(892, 549);
            this.panel5.TabIndex = 30;
            // 
            // ptbMain
            // 
            this.ptbMain.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ptbMain.Location = new System.Drawing.Point(3, 7);
            this.ptbMain.MinimumSize = new System.Drawing.Size(839, 537);
            this.ptbMain.Name = "ptbMain";
            this.ptbMain.Size = new System.Drawing.Size(886, 537);
            this.ptbMain.TabIndex = 0;
            this.ptbMain.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 647);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvPlate)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvPlate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox cbFilter;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTransaction;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox ptbMain;
    }
}

