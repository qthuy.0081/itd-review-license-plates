﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace ITD_Review_license__plates
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        #region Variables
        List<OUT_CheckSmartCard> transaction = new List<OUT_CheckSmartCard>();
        OUT_CheckSmartCard trans = new OUT_CheckSmartCard();

        Model1 db = new Model1();
        private string plate;
        //C:\Users\thanh\Downloads\HinhLan_HPE\HinhLan\01\2020\08\01\00\0101
        private string path = ConfigurationManager.AppSettings["ImagePath"].ToString()+ @"\{0}\{1}\{2}\{3}\{4}\{5}\{6}.JPG";
        private string imagePath = @"\test.jpg";
        #endregion

        #region Events
        private void Form1_Load(object sender, EventArgs e)
        {
            transaction = db.OUT_CheckSmartCard.ToList();
            LoadTransactions();
        }

        private void dtgvPlate_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            lblTransaction.Text = dtgvPlate.Rows[e.RowIndex].Cells[0].Value.ToString();
            
            if (String.IsNullOrEmpty(dtgvPlate.Rows[e.RowIndex].Cells[1].Value as String))
            {                
                lblDate.Text = "";
            }
            else
                BindingLabel(dtgvPlate.Rows[e.RowIndex].Cells[1].Value.ToString());
            trans = db.OUT_CheckSmartCard.Where(x => x.TransactionID == lblTransaction.Text).FirstOrDefault();
            loadImages(trans);
        }

        private void cbFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFilter.Checked)
            {
                // Filter here
                FilterTransByDigits();
            }
            else
                LoadTransactions();
        }
        #endregion

        #region Methods
        private void LoadTransactions()
        {
            var transactions = db.OUT_CheckSmartCard.Select(x => new {x.TransactionID, x.RecogPlateNumber}).ToList();
            dtgvPlate.DataSource = transactions;
            
        }

        private void BindingLabel(string plate)
        {
            string getDate = db.OUT_CheckSmartCard.Where(x => x.RecogPlateNumber == plate).Select(x => x.CheckDate).FirstOrDefault().ToString();
            //string getId = db.OUT_CheckSmartCard.Where(x => x.RecogPlateNumber == plate).Select(x => x.TransactionID).FirstOrDefault().ToString();

            lblDate.Text = getDate;
            
           // lblTransaction.Text = getId;

        }

        private void FilterTransByDigits()
        {
            var trans = db.OUT_CheckSmartCard.Select(x => new { x.TransactionID, x.RecogPlateNumber }).Where(x => x.RecogPlateNumber.Length < 6).ToList();
            dtgvPlate.DataSource = trans;
            dtgvPlate.Refresh();
        }

        private void loadImages(OUT_CheckSmartCard trans)
        {
            string transid = trans.TransactionID; //20200106171431548_0210
            // C:\Users\thanh\Downloads\HinhLan_HPE\HinhLan\01\2020\08\01\00\0101
            string station = trans.StationID?.ToString("00");
            string year = transid.Substring(0, 4);
            string month = transid.Substring(4, 2);
            string date = transid.Substring(6, 2);
            string hour = transid.Substring(8, 2);
            string imageid = trans.ImageID;
            string lane = trans.ImageID.Substring(18);
            string imageUrl = string.Format(path, station, year, month, date, hour, lane, imageid);
            if (File.Exists(imageUrl))
            {
                ptbMain.SizeMode = PictureBoxSizeMode.StretchImage;
                ptbMain.Load(imageUrl);
            }
            else
            {
                ptbMain.SizeMode = PictureBoxSizeMode.StretchImage;
                ptbMain.Load(Environment.CurrentDirectory + "/Resources/test.png");
            }    

        }
        #endregion

        
    }
}
